#version 410 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;

out vec3 vColor;

uniform mat4 projection;
uniform mat4 view;

void main()
{
  vColor = color;
  gl_Position = projection * view * vec4(position, 1.0f);
}