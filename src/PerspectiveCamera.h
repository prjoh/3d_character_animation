#pragma once


#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>


class PerspectiveCamera
{
  public:
    // camera attributes
    glm::vec3 position;
    glm::vec3 front;
    glm::vec3 right;
    glm::vec3 up;
    glm::vec3 world_up;
    float near;
    float far;
    float aspect;
    float fov;
    // euler angles
    float yaw_angle;
    float pitch_angle;

    PerspectiveCamera(glm::vec3 position, float fov, float aspect, float near, float far);
    void Move(glm::vec3 &distance);
    void SetPosition(glm::vec3 &position);
    void AddEulerRotation(float yaw_angle, float pitch_angle, bool constrain_pitch = true);
    void SetAspect(float aspect);
    void SetFOV(float fov);
    glm::mat4 ViewMatrix(void);
    glm::mat4 ProjectionMatrix(void);
};
