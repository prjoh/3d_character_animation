#include "Model.h"


Model::Model(ModelLoader &model_loader, bool gamma) : gamma_correction(gamma)
{
  model_loader.LoadMeshes(meshes);
  model_loader.LoadAnimations(animator);

//  using namespace Texture_Util;
//  for (auto joint : animator.joints)
//  {
//    std::vector<Vertex> vertices;
//    std::vector<unsigned int> indices;
//    // TODO: For a joint, generate a cube like main.vertices_data / main.indices
//    skeleton.insert(std::make_pair(joint.joint_id, Mesh(vertices, indices, std::vector<Texture>())));
//  }
}

void Model::Draw(ShaderProgram &shader)
{
  for (unsigned int i = 0; i < meshes.size(); ++i)
    meshes[i].Draw(shader);
}

// TODO: Make debug drawer store VAOs and update it during model draw?
void Model::DrawSkeleton(DebugDrawer &debug, glm::mat4 model_transform, float delta_time)
{
  glm::vec3 color = glm::vec3(0.0f, 1.0f, 0.0f);
  for (auto joint : animator.joints)
  {
//    glm::mat4 model = joint.transform; // TODO: joint.transform needs to be walked up the parent chain
//    shader.SetMat4f("model", model);
//    Mesh &mesh = skeleton.at(joint.joint_id);
//    mesh.Draw(shader);
    if (joint.parent_index > -1)
    {
      glm::vec3 from = glm::vec3((model_transform * animator.GetGlobalTransform(joint))[3]);
      glm::vec3 to = glm::vec3((model_transform * animator.GetGlobalTransform(animator.joints[joint.parent_index]))[3]);
      debug.DrawLine(from, to, color);
    }
  }
}
