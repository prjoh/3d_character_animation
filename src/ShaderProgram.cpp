#include "ShaderProgram.h"


ShaderProgram::ShaderProgram(std::string &v_shader_src_path, std::string &f_shader_src_path)
{
  std::string v_shader_src, f_shader_src;
  std::ifstream v_shader_src_sile, f_shader_src_file;
  // ensure ifstream objects can throw exceptions:
  v_shader_src_sile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
  f_shader_src_file.exceptions (std::ifstream::failbit | std::ifstream::badbit);
  try 
  {
    // open files
    v_shader_src_sile.open(v_shader_src_path);
    f_shader_src_file.open(f_shader_src_path);
    std::stringstream v_shader_src_stream, f_shader_src_stream;
    // read file's buffer contents into streams
    v_shader_src_stream << v_shader_src_sile.rdbuf();
    f_shader_src_stream << f_shader_src_file.rdbuf();
    // close file handlers
    v_shader_src_sile.close();
    f_shader_src_file.close();
    // convert stream into string
    v_shader_src = v_shader_src_stream.str();
    f_shader_src = f_shader_src_stream.str();
  }
  catch (std::ifstream::failure& e)
  {
    std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ: " << strerror(errno) << std::endl;
  }
  unsigned int v_shader, f_shader;
  const char *v_shader_src_c = v_shader_src.c_str();
  const char *f_shader_src_c = f_shader_src.c_str();
  // Compile shaders
  v_shader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(v_shader, 1, &v_shader_src_c, NULL);
  glCompileShader(v_shader);
  CheckCompileErrors(v_shader, "VERTEX");
  f_shader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(f_shader, 1, &f_shader_src_c, NULL);
  glCompileShader(f_shader);
  CheckCompileErrors(f_shader, "FRAGMENT");
  // Link to shader program
  this->id = glCreateProgram();
  glAttachShader(this->id, v_shader);
  glAttachShader(this->id, f_shader);
  glLinkProgram(this->id);
  CheckCompileErrors(this->id, "PROGRAM");
  // Delete shaders
  glDeleteShader(v_shader);
  glDeleteShader(f_shader);
}

void ShaderProgram::Use(void) 
{ 
  glUseProgram(this->id); 
}

void ShaderProgram::SetInt(const std::string &name, int data) const
{ 
  glUniform1i(glGetUniformLocation(this->id, name.c_str()), data); 
}

void ShaderProgram::SetFloat(const std::string &name, float data) const
{ 
  glUniform1f(glGetUniformLocation(this->id, name.c_str()), data); 
}

void ShaderProgram::SetVec3fv(const std::string &name, const glm::vec3 &data) const
{ 
  glUniform3fv(glGetUniformLocation(this->id, name.c_str()), 1, &data[0]); 
}

void ShaderProgram::SetVec3f(const std::string &name, float x, float y, float z) const
{ 
  glUniform3f(glGetUniformLocation(this->id, name.c_str()), x, y, z); 
}

void ShaderProgram::SetVec4fv(const std::string &name, const glm::vec4 &data) const
{ 
  glUniform4fv(glGetUniformLocation(this->id, name.c_str()), 1, &data[0]); 
}

void ShaderProgram::SetVec4f(const std::string &name, float x, float y, float z, float w) const
{ 
  glUniform4f(glGetUniformLocation(this->id, name.c_str()), x, y, z, w); 
}

void ShaderProgram::SetMat2f(const std::string &name, glm::mat2 &data) const
{
  glUniformMatrix2fv(glGetUniformLocation(this->id, name.c_str()), 1, GL_FALSE, &data[0][0]);
}

void ShaderProgram::SetMat3f(const std::string &name, glm::mat3 &data) const
{
  glUniformMatrix3fv(glGetUniformLocation(this->id, name.c_str()), 1, GL_FALSE, &data[0][0]);
}

void ShaderProgram::SetMat4f(const std::string &name, glm::mat4 &data) const
{
  glUniformMatrix4fv(glGetUniformLocation(this->id, name.c_str()), 1, GL_FALSE, &data[0][0]);
}

void ShaderProgram::CheckCompileErrors(unsigned int shader, std::string type)
{
  int success;
  char info_log[1024];
  if (type != "PROGRAM")
  {
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
      glGetShaderInfoLog(shader, 1024, NULL, info_log);
      std::cout << "ERROR::SHADER_COMPILATION_ERROR of type: " << type << "\n" << info_log << "\n -- --------------------------------------------------- -- " << std::endl;
    }
  }
  else
  {
    glGetProgramiv(shader, GL_LINK_STATUS, &success);
    if (!success)
    {
      glGetProgramInfoLog(shader, 1024, NULL, info_log);
      std::cout << "ERROR::PROGRAM_LINKING_ERROR of type: " << type << "\n" << info_log << "\n -- --------------------------------------------------- -- " << std::endl;
    }
  }
}
