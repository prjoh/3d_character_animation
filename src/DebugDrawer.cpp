#include "DebugDrawer.h"


DebugDrawer::DebugDrawer(ShaderProgram &shader_program) : shader_program(shader_program) {

}

void DebugDrawer::SetMatrices(glm::mat4 &view_matrix, glm::mat4 &projection_matrix)
{
  shader_program.Use();
  shader_program.SetMat4f("view", view_matrix);
  shader_program.SetMat4f("projection", projection_matrix);
}

void DebugDrawer::DrawLine(glm::vec3 &from, glm::vec3 &to, glm::vec3 &color)
{
  GLfloat points[12] = {
    from.x, from.y, from.z, color.x, color.y, color.z,
    to.x, to.y, to.z, color.x, color.y, color.z,
  };

  unsigned int vao, vbo;
  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);

  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(points), &points, GL_STATIC_DRAW);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), 0);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
  glDrawArrays(GL_LINES, 0, 2);
  glBindVertexArray(0);
}
