#include <iostream>
#include <sdl/SDL.h>
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

#include "PerspectiveCamera.h"
#include "Model.h"
#include "DebugDrawer.h"


using namespace Texture_Util;


// Core loop flag
bool running = true;

// settings
unsigned int screen_width = 800;
unsigned int screen_height = 600;
float sensitivity = 0.1f;

// camera
float fov = 60.0f;
float aspect = (float)screen_width / (float)screen_height;
float near = 0.01f;
float far = 1000.0f;
PerspectiveCamera camera(glm::vec3(0.0f, 0.0f, 5.0f), fov, aspect, near, far);

// timing
float delta_time = 0.0f; // time between current frame and last frame in ms
float last_frame = 0.0f;

// resource paths
std::string shader_paths[] = {
  "Shaders/tutorial/multiple_lights",
  "Shaders/tutorial/light_cube",
  "Shaders/tutorial/model_loading",
  "Shaders/debug_draw",
};
std::string model_paths[] = {
  "Models/erika/erika.fbx",
  "Models/castle_guard/castle_guard1.fbx",
};

// resources
std::map<std::string, ShaderProgram> shaders;
std::map<std::string, Model> models;

float vertices_data[] = {
  // positions          // normals           // texture coords
  -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,
  0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 0.0f,
  0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
  -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 1.0f,

  -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,
  0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 0.0f,
  0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
  -0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 1.0f,

  -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
  -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
  -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
  -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 0.0f,

  0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
  0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
  0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
  0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 0.0f,

  -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,
  0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 1.0f,
  0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
  -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 0.0f,

  -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f,
  0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 1.0f,
  0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
  -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 0.0f,
};
std::vector<unsigned int> indeces {
  0, 1, 3,
  1, 2, 3,
  5, 6, 4,
  6, 7, 4,
  9, 10, 8,
  10, 11, 8,
  13, 14, 12,
  14, 15, 12,
  17, 18, 16,
  18, 19, 16,
  21, 22, 20,
  22, 23, 20,
};
unsigned int vertices_data_size = *(&vertices_data + 1) - vertices_data;
glm::vec3 cubePositions[] = {
  glm::vec3( 0.0f,  0.0f,  0.0f),
  glm::vec3( 2.0f,  5.0f, -15.0f),
  glm::vec3(-1.5f, -2.2f, -2.5f),
  glm::vec3(-3.8f, -2.0f, -12.3f),
  glm::vec3( 2.4f, -0.4f, -3.5f),
  glm::vec3(-1.7f,  3.0f, -7.5f),
  glm::vec3( 1.3f, -2.0f, -2.5f),
  glm::vec3( 1.5f,  2.0f, -2.5f),
  glm::vec3( 1.5f,  0.2f, -1.5f),
  glm::vec3(-1.3f,  1.0f, -1.5f)
};
// positions of the point lights
glm::vec3 pointLightPositions[] = {
  glm::vec3( 0.7f,  0.2f,  2.0f),
  glm::vec3( 2.3f, -3.3f, -4.0f),
  glm::vec3(-4.0f,  2.0f, -12.0f),
  glm::vec3( 0.0f,  0.0f, -3.0f)
};


void processInput(void)
{
  const Uint8 *keystate = SDL_GetKeyboardState(NULL);

  if (keystate[SDL_SCANCODE_ESCAPE])
  {
    running = false;
    return;
  }
  float camera_speed = 2.5f * delta_time / 1000.0f;
  glm::vec3 camera_move_distance;
  if (keystate[SDL_SCANCODE_LEFT] || keystate[SDL_SCANCODE_A])
  {
    camera_move_distance = -camera.right * camera_speed;
    camera.Move(camera_move_distance);
  }
  if (keystate[SDL_SCANCODE_RIGHT] || keystate[SDL_SCANCODE_D])
  {
    camera_move_distance = camera.right * camera_speed;
    camera.Move(camera_move_distance);
  }
  if (keystate[SDL_SCANCODE_UP] || keystate[SDL_SCANCODE_W])
  {
    camera_move_distance = camera_speed * camera.front;
    camera.Move(camera_move_distance);
  }
  if (keystate[SDL_SCANCODE_DOWN] || keystate[SDL_SCANCODE_S])
  {
    camera_move_distance = -camera_speed * camera.front;
    camera.Move(camera_move_distance);
  }

  // Mouse and other general SDL events (eg. windowing)
  SDL_Event event;
  while(SDL_PollEvent(&event))
  {
    switch (event.type)
    {
      case SDL_WINDOWEVENT:
      {
        if (event.window.event == SDL_WINDOWEVENT_RESIZED)
        {
          screen_width = event.window.data1;
          screen_height = event.window.data2;
          aspect = (float)screen_width / (float)screen_height;
          camera.SetAspect(aspect);
        }
        break;
      }
      case SDL_MOUSEMOTION:
      {
        camera.AddEulerRotation(event.motion.xrel * sensitivity, -event.motion.yrel * sensitivity);
        break;
      }
      case SDL_MOUSEWHEEL:
      {
        fov -= event.wheel.y;
        if (fov < 1.0f)
          fov = 1.0f;
        if (fov > 60.0f)
          fov = 60.0f;
        camera.SetFOV(fov);
        break;
      }
      case SDL_QUIT:
      {
        running = false; //quit
        break;
      }
    }
  }
}

int main (int argc, char *argv[])
{
  SDL_Window *window;                    // Declare a pointer
  SDL_Init(SDL_INIT_VIDEO);              // Initialize SDL2

#ifdef __APPLE__
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 32);
#elif __WIN32__
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 6);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
#endif

  // Create an application window with the following settings:
  window = SDL_CreateWindow(
    "3D RPG",                               // window title
    SDL_WINDOWPOS_UNDEFINED,                // initial x position
    SDL_WINDOWPOS_UNDEFINED,                // initial y position
    screen_width,                           // width, in pixels
    screen_height,                          // height, in pixels
    SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE|SDL_WINDOW_MAXIMIZED  // flags - see below
  );

  // Check that the window was successfully created
  if (window == NULL) {
    // In the case that the window could not be made...
    std::cout << "Could not create window: " << SDL_GetError() << std::endl;
    return 1;
  }

  // Create an OpenGL context associated with the window.
  SDL_GLContext gl_context = SDL_GL_CreateContext(window);

  // Disable VSYNC
  // SDL_GL_SetSwapInterval(0);

  // Hides cursor and tells driver to report continuous motion in the current window
  SDL_SetRelativeMouseMode(SDL_TRUE);

  // Load GL extensions using glad
  if (!gladLoadGLLoader((GLADloadproc) SDL_GL_GetProcAddress))
  {
    std::cout << "Failed to initialize the OpenGL context." << std::endl;
    exit(1);
  }
  std::cout << "OpenGL Version " << GLVersion.major << "." << GLVersion.minor << " loaded." << std::endl;

  // configure global opengl state
  // -----------------------------
  glEnable(GL_DEPTH_TEST);

  int nr_attributes;
  glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nr_attributes);
  std::cout << "Maximum nr of vertex attributes supported: " << nr_attributes << std::endl;

  char *base_path = SDL_GetBasePath();
  char *data_path = nullptr;
  if (base_path) {
    data_path = base_path;
  } else {
    data_path = SDL_strdup("./");
  }
  std::cout << "SDL DATA PATH: " << data_path << std::endl;

  // build and compile shader programs
  // ------------------------------------
  for (auto &shader_path : shader_paths)
  {
    size_t next, last = 0;
    while ((next = shader_path.find("/", last)) != std::string::npos) {
      last = next + 1;
    }
    std::cout << "PROCESSING SHADER: " << shader_path.substr(last) << std::endl;
    std::string shader_name = shader_path.substr(last);
    std::string vs_src = std::string(data_path) + shader_path + ".vs";
    std::string fs_src = std::string(data_path) + shader_path + ".fs";
    shaders.insert(std::make_pair(shader_name, ShaderProgram(vs_src, fs_src)));
  }
  ShaderProgram lighting_shader = shaders.at("multiple_lights");
  ShaderProgram debug_shader = shaders.at("debug_draw");
  ShaderProgram light_cube_shader = shaders.at("light_cube");
  ShaderProgram model_shader = shaders.at("model_loading");

  // tell stb_image.h to flip loaded texture's on the y-axis (before loading model).
  // stbi_set_flip_vertically_on_load(true);

  // load models
  // -----------
  for (auto &model_path : model_paths)
  {
    size_t next, last = 0;
    while ((next = model_path.find("/", last)) != std::string::npos) {
      last = next + 1;
    }
    std::cout << "PROCESSING MODEL: " << model_path.substr(last) << std::endl;
    std::string model_name = model_path.substr(last);
    ModelLoader loader(std::string(data_path) + model_path);
    models.insert(std::make_pair(model_name, Model(loader)));
  }
  Model our_model = models.at("erika.fbx");
  Model castle_guard = models.at("castle_guard1.fbx");

  // Instance for drawing debug shapes
  DebugDrawer debug_drawer(debug_shader);

  // shader configuration
  // --------------------
  lighting_shader.Use();
  lighting_shader.SetInt("material.diffuse", 0);
  lighting_shader.SetInt("material.specular", 1);
  lighting_shader.SetFloat("material.shininess", 32.0f);
  /*
     Here we set all the uniforms for the 5/6 types of lights we have. We have to set them manually and index
     the proper PointLight struct in the array to set each uniform variable. This can be done more code-friendly
     by defining light types as classes and set their values in there, or by using a more efficient uniform approach
     by using 'Uniform buffer objects', but that is something we'll discuss in the 'Advanced GLSL' tutorial.
  */
  // directional light
  lighting_shader.SetVec3f("dirLight.direction", -0.2f, -1.0f, -0.3f);
  lighting_shader.SetVec3f("dirLight.ambient", 0.05f, 0.05f, 0.05f);
  lighting_shader.SetVec3f("dirLight.diffuse", 0.4f, 0.4f, 0.4f);
  lighting_shader.SetVec3f("dirLight.specular", 0.5f, 0.5f, 0.5f);
  // point lights
  glm::vec3 diffuse_color = glm::vec3(0.25, 0.93, 0.18);
  for (GLuint i = 0; i < 4; i++)
  {
    std::string number = std::to_string(i);
    lighting_shader.SetVec3fv("pointLights[" + number + "].position", pointLightPositions[i]);
    lighting_shader.SetVec3f("pointLights[" + number + "].ambient", 0.05f, 0.05f, 0.05f);
    lighting_shader.SetVec3fv("pointLights[" + number + "].diffuse", diffuse_color);
    lighting_shader.SetVec3f("pointLights[" + number + "].specular", 1.0f, 1.0f, 1.0f);
    lighting_shader.SetFloat("pointLights[" + number + "].constant", 1.0f);
    lighting_shader.SetFloat("pointLights[" + number + "].linear", 0.09);
    lighting_shader.SetFloat("pointLights[" + number + "].quadratic", 0.032);
  }
  // spotLight
  lighting_shader.SetVec3fv("spotLight.position", camera.position);
  lighting_shader.SetVec3fv("spotLight.direction", camera.front);
  lighting_shader.SetVec3f("spotLight.ambient", 0.0f, 0.0f, 0.0f);
  lighting_shader.SetVec3f("spotLight.diffuse", 1.0f, 1.0f, 1.0f);
  lighting_shader.SetVec3f("spotLight.specular", 1.0f, 1.0f, 1.0f);
  lighting_shader.SetFloat("spotLight.constant", 1.0f);
  lighting_shader.SetFloat("spotLight.linear", 0.09);
  lighting_shader.SetFloat("spotLight.quadratic", 0.032);
  lighting_shader.SetFloat("spotLight.cutOff", glm::cos(glm::radians(12.5f)));
  lighting_shader.SetFloat("spotLight.outerCutOff", glm::cos(glm::radians(15.0f)));

  // set up vertex data (and buffer(s)) and configure vertex attributes
  // ------------------------------------------------------------------
  std::vector<Vertex> vertices;
  for (unsigned int i = 0; i < vertices_data_size/8; ++i)
  {
    Vertex vertex;
    vertex.position = glm::vec3(vertices_data[i * 8], vertices_data[i * 8+1], vertices_data[i * 8+2]);
    vertex.normal = glm::vec3(vertices_data[i * 8+3], vertices_data[i * 8+4], vertices_data[i * 8+5]);
    vertex.tex_coords = glm::vec2(vertices_data[i * 8+6], vertices_data[i * 8+7]);
    vertices.push_back(vertex);
  }

  // load textures (we now use a utility function to keep the code more organized)
  // -----------------------------------------------------------------------------
  Texture diffuse_texture, specular_texture;
  std::string light_map_path(data_path), light_map_path2(data_path),light_map_path3(data_path);
  diffuse_texture.path = light_map_path.append("Textures/container2.png");
  diffuse_texture.id = load_texture_from_file(diffuse_texture.path.c_str());
  diffuse_texture.type = "texture_diffuse";
  specular_texture.path = light_map_path2.append("Textures/container2_specular.png");
  specular_texture.id = load_texture_from_file(specular_texture.path.c_str());
  specular_texture.type = "texture_specular";
  std::vector<Texture> cube_textures;
  cube_textures.push_back(diffuse_texture);
  cube_textures.push_back(specular_texture);

  // Create cubes
  std::vector<Mesh> cubes;
  for (unsigned int i = 0; i < 10; ++i)
    cubes.push_back(Mesh(vertices, indeces, cube_textures));
  // Create light cubes
  std::vector<Mesh> light_cubes;
  for (unsigned int i = 0; i < 4; ++i)
    light_cubes.push_back(Mesh(vertices, indeces, cube_textures));


  // Core loop
  while (running)
  {
    // per-frame time logic
    // --------------------
    float current_frame = SDL_GetTicks();
    delta_time = current_frame - last_frame;
    last_frame = current_frame;
    std::cout << "Frame time: " << delta_time << "ms" << std::flush << "\r";

    processInput();

    /* do some other stuff here -- draw your app, etc. */
    /* Clear context */
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // MVP transformations
    glm::mat4 projection = camera.ProjectionMatrix();
    glm::mat4 view = camera.ViewMatrix();
    glm::mat4 model = glm::mat4(1.0f);

    // Debug drawing
//    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE ); // TODO: Uncomment for debug view!
    debug_drawer.SetMatrices(view, projection);
    glm::vec3 from = glm::vec3(0.5f, -0.5f, 1.0f);
    glm::vec3 to = glm::vec3(-0.5f, 0.5f, 1.0f);
    glm::vec3 color = glm::vec3(1.0f, 0.0f, 0.0f);
    debug_drawer.DrawLine(from, to , color);

    // Draw cubes
    lighting_shader.Use();
    lighting_shader.SetVec3fv("viewPos", camera.position);
    lighting_shader.SetMat4f("projection", projection);
    lighting_shader.SetMat4f("view", view);
    for (unsigned int i = 0; i < cubes.size(); ++i)
    {
      model = glm::mat4(1.0f);
      model = glm::translate(model, cubePositions[i]);
      float angle = 20.0f * i;
      model = glm::rotate(model, glm::radians(angle) + current_frame/1000.0f, glm::vec3(1.0f, 0.3f, 0.5f));
      lighting_shader.SetMat4f("model", model);
      cubes[i].Draw(lighting_shader);
    }

    // Draw light cubes
    light_cube_shader.Use();
    light_cube_shader.SetMat4f("projection", projection);
    light_cube_shader.SetMat4f("view", view);
    for (unsigned int i = 0; i < 4; ++i)
    {
      model = glm::mat4(1.0f);
      model = glm::translate(model, pointLightPositions[i]);
      model = glm::scale(model, glm::vec3(0.2f)); // Make it a smaller cube
      light_cube_shader.SetMat4f("model", model);
      light_cubes[i].Draw(light_cube_shader);
    }

    // Draw models
    // erika
    model_shader.Use();
    model_shader.SetMat4f("projection", projection);
    model_shader.SetMat4f("view", view);
    model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(-5.0f, -2.0f, -2.0f)); // translate it down so it's at the center of the scene
    model = glm::rotate(model, current_frame/1000.0f * 0.5f, glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::scale(model, glm::vec3(0.02f, 0.02f, 0.02f)); // it's a bit too big for our scene, so scale it down
    model_shader.SetMat4f("model", model);
    our_model.Draw(model_shader);
//    debug_shader.Use();
//    our_model.DrawSkeleton(debug_drawer, model, 0);
    // castle guard
    model_shader.Use();
    model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(5.0f, -2.0f, -2.0f)); // translate it down so it's at the center of the scene
    model = glm::rotate(model, -current_frame/1000.0f * 0.5f, glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f)); // it's a bit too big for our scene, so scale it down
    model_shader.SetMat4f("model", model);
    castle_guard.Draw(model_shader);

    /* Swap our buffer to display the current contents of buffer on screen */ 
    SDL_GL_SwapWindow(window);
  }

  // Once finished with OpenGL functions, the SDL_GLContext can be deleted.
  SDL_GL_DeleteContext(gl_context);  

  // Close and destroy the window
  SDL_DestroyWindow(window);

  // Clean up
  SDL_Quit();
  return 0;
}
