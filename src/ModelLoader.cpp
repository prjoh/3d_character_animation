#include "ModelLoader.h"


using namespace Texture_Util;


ModelLoader::ModelLoader(const std::string &path)
{
  std::cout << "LOADING FILE FROM: " << path <<  std::endl;

  // read file via ASSIMP
  scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
  // check for errors
  if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
  {
    std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
    return;
  }

  // retrieve the directory path of the filepath
  directory = path.substr(0, path.find_last_of('/'));
}

void ModelLoader::LoadMeshes(std::vector<Mesh> &meshes)
{
  // process ASSIMP's root node recursively
  ProcessMeshNode(scene->mRootNode, scene, meshes);
}

void ModelLoader::LoadAnimations(Animator &animator) {
  // Create Joint vector from aiNode hierarchy
  ProcessJointNode(scene->mRootNode, scene, animator.joints, -1);

  std::set<std::string> joint_names_debug;

  std::cout << "Num of Animation: " << scene->mNumAnimations <<  std::endl;
  // Process Animation data
  animator.animations.reserve(scene->mNumAnimations);
  for (unsigned int i = 0; i < scene->mNumAnimations; ++i)
  {
    aiAnimation *ai_anim = scene->mAnimations[i];
    std::cout << "Process Animation: " << ai_anim->mName.C_Str() <<  std::endl;
    Animation anim;
    anim.name = ai_anim->mName.C_Str();
    anim.duration = ai_anim->mDuration;
    anim.ticks_per_second = ai_anim->mTicksPerSecond;
    animator.animations.push_back(anim);
    std::map<std::pair<int, double>, JointKeyframe> joint_keyframes;
    for (unsigned int j = 0; j < ai_anim->mNumChannels; ++j)
    {
      aiNodeAnim *node_anim = ai_anim->mChannels[j];
      assert(node_anim->mNumPositionKeys == node_anim->mNumRotationKeys &&
             node_anim->mNumRotationKeys == node_anim->mNumScalingKeys);
      std::string joint_name = node_anim->mNodeName.C_Str();
      joint_names_debug.insert(joint_name);
      std::cout << "  " << joint_name << ": " << ai_anim->mChannels[j]->mNumPositionKeys << std::endl;
      auto it = std::find_if(animator.joints.begin(), animator.joints.end(), [&joint_name](const Joint &joint){ return joint.name == joint_name; });
      assert(it != animator.joints.end());
      int joint_id = it - animator.joints.begin();
      for (unsigned int k = 0; k < ai_anim->mChannels[j]->mNumPositionKeys; ++k)
      {
        assert(node_anim->mPositionKeys[k].mTime == node_anim->mRotationKeys[k].mTime &&
               node_anim->mRotationKeys[k].mTime == node_anim->mScalingKeys[k].mTime);
        double time = node_anim->mPositionKeys[k].mTime;
        auto it = std::find(anim.times.begin(), anim.times.end(), time);
        if (it == anim.times.end())
          anim.times.push_back(time);
        JointKeyframe joint_key;
        joint_key.joint_id = joint_id;
        joint_key.translation = vec3_cast(node_anim->mPositionKeys[k].mValue);
        joint_key.scale = vec3_cast(node_anim->mScalingKeys[k].mValue);
        joint_key.rotation = quat_cast(node_anim->mRotationKeys[k].mValue);
        joint_keyframes.insert(std::make_pair(std::make_pair(joint_id, time), joint_key));
      }
    }
    std::cout << "  NUM OF TIMES: " << anim.times.size() << std::endl;
    std::sort(anim.times.begin(), anim.times.end());

    for (unsigned int j = 0; j < anim.times.size(); ++j)
    {
      if (anim.keyframes.size() < anim.times.size())
        anim.keyframes.push_back(std::vector<JointKeyframe>());
      double time = anim.times[j];
      for (auto joint : animator.joints)
      {
        auto key = std::make_pair(joint.joint_id, time);
        if (joint_keyframes.find(key) == joint_keyframes.end())
          continue;
        anim.keyframes[j].push_back(joint_keyframes[key]);
      }
    }

    std::cout << "  NUM OF UNIQUE ANIMATION JOINTS: " << joint_names_debug.size() << std::endl;
    std::cout << "  NUM OF ANIMATOR JOINTS: " << animator.joints.size() << std::endl;
  }
}

void ModelLoader::ProcessJointNode(aiNode *node, const aiScene *scene, std::vector<Joint> &joints, int parent_index)
{
  // process Joint for current node
  Joint joint;
  joint.joint_id = joints.size();
  joint.name = node->mName.C_Str();
  joint.parent_index = parent_index;
  joint.transform = mat4_cast(node->mTransformation);
  bool bone_found = false;
  for (unsigned int i = 0; i < node->mNumMeshes; ++i)
  {
    aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
    for (unsigned int i = 0; i < mesh->mNumBones; ++i)
    {
      aiBone *bone = mesh->mBones[i];
      if (bone->mName == node->mName)
      {
        bone_found = true;
        joint.offset_matrix = mat4_cast(bone->mOffsetMatrix);
        break;
      }
    }

    if (bone_found)
      break;
  }
  joints.push_back(joint);

  // after we've processed all of the meshes (if any) we then recursively process each of the children nodes
  for (unsigned int i = 0; i < node->mNumChildren; ++i)
    ProcessJointNode(node->mChildren[i], scene, joints, joint.joint_id);
}

// processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
void ModelLoader::ProcessMeshNode(aiNode *node, const aiScene *scene, std::vector<Mesh> &meshes)
{
  // process each mesh located at the current node
  for (unsigned int i = 0; i < node->mNumMeshes; ++i)
  {
    // the node object only contains indices to index the actual objects in the scene.
    // the scene contains all the data, node is just to keep stuff organized (like relations between nodes).
    aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
    meshes.push_back(ProcessMesh(mesh, scene));
  }
  // after we've processed all of the meshes (if any) we then recursively process each of the children nodes
  for (unsigned int i = 0; i < node->mNumChildren; ++i)
    ProcessMeshNode(node->mChildren[i], scene, meshes);
}

Mesh ModelLoader::ProcessMesh(aiMesh *mesh, const aiScene *scene)
{
  // data to fill
  std::vector<Vertex> vertices;
  std::vector<unsigned int> indices;
  std::vector<Texture> textures;

  // walk through each of the mesh's vertices
  for (unsigned int i = 0; i < mesh->mNumVertices; ++i)
  {
    Vertex vertex;
    glm::vec3 vector; // we declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
    // positions
    vector.x = mesh->mVertices[i].x;
    vector.y = mesh->mVertices[i].y;
    vector.z = mesh->mVertices[i].z;
    vertex.position = vector;
    // normals
    if (mesh->HasNormals())
    {
      vector.x = mesh->mNormals[i].x;
      vector.y = mesh->mNormals[i].y;
      vector.z = mesh->mNormals[i].z;
      vertex.normal = vector;
    }
    // texture coordinates
    if (mesh->mTextureCoords[0]) // does the mesh contain texture coordinates?
    {
      glm::vec2 vec;
      // a vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't
      // use models where a vertex can have multiple texture coordinates so we always take the first set (0).
      vec.x = mesh->mTextureCoords[0][i].x;
      vec.y = mesh->mTextureCoords[0][i].y;
      vertex.tex_coords = vec;
      // tangent
      vector.x = mesh->mTangents[i].x;
      vector.y = mesh->mTangents[i].y;
      vector.z = mesh->mTangents[i].z;
      vertex.tangent = vector;
      // bitangent
      vector.x = mesh->mBitangents[i].x;
      vector.y = mesh->mBitangents[i].y;
      vector.z = mesh->mBitangents[i].z;
      vertex.bitangent = vector;
    }
    else
      vertex.tex_coords = glm::vec2(0.0f, 0.0f);

    vertices.push_back(vertex);
  }
  // now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
  for (unsigned int i = 0; i < mesh->mNumFaces; ++i)
  {
    aiFace face = mesh->mFaces[i];
    // retrieve all indices of the face and store them in the indices vector
    for (unsigned int j = 0; j < face.mNumIndices; ++j)
      indices.push_back(face.mIndices[j]);
  }
  // process materials
  aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
  // we assume a convention for sampler names in the shaders. Each diffuse texture should be named
  // as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER.
  // Same applies to other texture as the following list summarizes:
  // diffuse: texture_diffuseN
  // specular: texture_specularN
  // emission: texture_emissionN
  // normal: texture_normalN

  // 1. diffuse maps
  std::vector<Texture> diffuse_maps = LoadMaterialTextures(material, scene, aiTextureType_DIFFUSE, "texture_diffuse");
  textures.insert(textures.end(), diffuse_maps.begin(), diffuse_maps.end());
  // 2. specular maps
  std::vector<Texture> specular_maps = LoadMaterialTextures(material, scene, aiTextureType_SPECULAR, "texture_specular");
  textures.insert(textures.end(), specular_maps.begin(), specular_maps.end());
  // 3. emission maps
  std::vector<Texture> emission_maps = LoadMaterialTextures(material, scene, aiTextureType_EMISSIVE, "texture_emission");
  textures.insert(textures.end(), emission_maps.begin(), emission_maps.end());
  // 4. normal maps
  std::vector<Texture> normal_maps = LoadMaterialTextures(material, scene, aiTextureType_NORMALS, "texture_normal");
  textures.insert(textures.end(), normal_maps.begin(), normal_maps.end());
  // // 5. height maps
  // std::vector<Texture> height_maps = LoadMaterialTextures(material, aiTextureType_AMBIENT, "texture_height");
  // textures.insert(textures.end(), height_maps.begin(), height_maps.end());

  // return a mesh object created from the extracted mesh data
  return Mesh(vertices, indices, textures);
}

// checks all material textures of a given type and loads the textures if they're not loaded yet.
// the required info is returned as a Texture struct.
std::vector<Texture> ModelLoader::LoadMaterialTextures(aiMaterial *mat, const aiScene *scene, aiTextureType type, std::string type_name)
{
  std::vector<Texture> textures;
  for (unsigned int i = 0; i < mat->GetTextureCount(type); ++i)
  {
    aiString str;
    mat->GetTexture(type, i, &str);
    // check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
    bool skip = false;
    for (unsigned int j = 0; j < textures_loaded.size(); ++j)
    {
      if (std::strcmp(textures_loaded[j].path.data(), str.C_Str()) == 0)
      {
        textures.push_back(textures_loaded[j]);
        skip = true; // a texture with the same filepath has already been loaded, continue to next one. (optimization)
        break;
      }
    }

    if (skip)
      continue;

    // if texture hasn't been loaded already, load it
    Texture texture;
    // check if scene contains embedded texture
    if (auto ai_texture = scene->GetEmbeddedTexture(str.C_Str()))
    {
      std::cout << "Loading texture from memory: " << str.C_Str() << std::endl;
      texture.id = load_texture_from_memory(ai_texture);
    }
    else
    {
      std::string path(this->directory);
      std::string filename(str.C_Str());
      // Cleanup if filename is a path/to/filename
      size_t pos = 0;
      while ((pos = filename.find("/")) != std::string::npos)
        filename.erase(0, pos + 1);
      std::cout << "Loading texture from file: " << path << "/" << filename << std::endl;
      texture.id = load_texture_from_file(path.append("/" + filename).c_str());
    }
    texture.type = type_name;
    texture.path = str.C_Str();
    textures.push_back(texture);
    textures_loaded.push_back(texture);  // store it as texture loaded for entire model, to ensure we won't unnecesery load duplicate textures.
  }
  return textures;
}
