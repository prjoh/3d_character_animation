#include <iostream>
#include <SDL.h>
#include <glad/glad.h>
#include <stb/stb_image.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "ShaderProgram.h"
#include "PerspectiveCamera.h"

#include <glm/gtx/string_cast.hpp>
// #include <assimp/Importer.hpp>      // C++ importer interface
// #include <assimp/scene.h>           // Output data structure
// #include <assimp/postprocess.h>     // Post processing flags

// Core loop flag
bool running = true;

// settings
unsigned int screen_width = 800;
unsigned int screen_height = 600;
float sensitivity = 0.1f;

// camera
float fov = 60.0f;
float aspect = (float)screen_width / (float)screen_height;
float near = 0.01f;
float far = 1000.0f;
PerspectiveCamera camera(glm::vec3(0.0f, 0.0f, 3.0f), fov, aspect, near, far);

// timing
float delta_time = 0.0f; // time between current frame and last frame in ms
float last_frame = 0.0f;

// lighting
glm::vec3 light_pos(1.2f, 0.0f, 2.0f);

void processInput(void)
{
  const Uint8 *keystate = SDL_GetKeyboardState(NULL);

  if (keystate[SDL_SCANCODE_ESCAPE])
  {
    running = false;
    return;
  }
  float camera_speed = 2.5f * delta_time / 1000.0f;
  glm::vec3 camera_move_distance;
  if (keystate[SDL_SCANCODE_LEFT] || keystate[SDL_SCANCODE_A])
  {
    camera_move_distance = -camera.right * camera_speed;
    camera.Move(camera_move_distance);
  }
  if (keystate[SDL_SCANCODE_RIGHT] || keystate[SDL_SCANCODE_D])
  {
    camera_move_distance = camera.right * camera_speed;
    camera.Move(camera_move_distance);
  }
  if (keystate[SDL_SCANCODE_UP] || keystate[SDL_SCANCODE_W])
  {
    camera_move_distance = camera_speed * camera.front;
    camera.Move(camera_move_distance);
  }
  if (keystate[SDL_SCANCODE_DOWN] || keystate[SDL_SCANCODE_S])
  {
    camera_move_distance = -camera_speed * camera.front;
    camera.Move(camera_move_distance);
  }
  
  // Mouse and other general SDL events (eg. windowing)
  SDL_Event event;
  while(SDL_PollEvent(&event))
  {
    switch (event.type)
    {
      case SDL_WINDOWEVENT:
      {
        if (event.window.event == SDL_WINDOWEVENT_RESIZED)
        {
          screen_width = event.window.data1;
          screen_height = event.window.data2;
          aspect = (float)screen_width / (float)screen_height;
          camera.SetAspect(aspect);
        }
        break;
      }
      case SDL_MOUSEMOTION:
      {
        camera.AddEulerRotation(event.motion.xrel * sensitivity, -event.motion.yrel * sensitivity);
        break;
      }
      case SDL_MOUSEWHEEL:
      {
        fov -= event.wheel.y;
        if (fov < 1.0f)
          fov = 1.0f;
        if (fov > 60.0f)
          fov = 60.0f;
        camera.SetFOV(fov);
        break;
      }
      case SDL_QUIT:
      {
        running = false; //quit
        break;
      }
    }
  }
}

unsigned int load_texture(char const * path)
{
  unsigned int texture_id;
  glGenTextures(1, &texture_id);
  
  int width, height, nr_components;
  unsigned char *data = stbi_load(path, &width, &height, &nr_components, 0);
  if (data)
  {
    GLenum format;
    if (nr_components == 1)
      format = GL_RED;
    else if (nr_components == 3)
      format = GL_RGB;
    else if (nr_components == 4)
      format = GL_RGBA;
    else
    {
      std::cout << "Invalid number of components in texture: " << path << std::endl;
      stbi_image_free(data);
      return texture_id;
    }

    glBindTexture(GL_TEXTURE_2D, texture_id);
    glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    stbi_image_free(data);
  }
  else
  {
    std::cout << "Texture failed to load at path: " << path << std::endl;
    stbi_image_free(data);
  }

  return texture_id;
}

int main (int argc, char *argv[])
{
  SDL_Window *window;                    // Declare a pointer

  SDL_Init(SDL_INIT_VIDEO);              // Initialize SDL2

#ifdef __APPLE__
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
#endif

  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 32);

  // Create an application window with the following settings:
  window = SDL_CreateWindow(
    "3D RPG",                               // window title
    SDL_WINDOWPOS_UNDEFINED,                // initial x position
    SDL_WINDOWPOS_UNDEFINED,                // initial y position
    screen_width,                           // width, in pixels
    screen_height,                          // height, in pixels
    SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE|SDL_WINDOW_MAXIMIZED  // flags - see below
  );

  // Check that the window was successfully created
  if (window == NULL) {
    // In the case that the window could not be made...
    std::cout << "Could not create window: " << SDL_GetError() << std::endl;
    return 1;
  }

  // Create an OpenGL context associated with the window.
  SDL_GLContext gl_context = SDL_GL_CreateContext(window);

  // Disable VSYNC
  // SDL_GL_SetSwapInterval(0);
  
  // Hides cursor and tells driver to report continuous motion in the current window
  SDL_SetRelativeMouseMode(SDL_TRUE);

  // Load GL extensions using glad
  if (!gladLoadGLLoader((GLADloadproc) SDL_GL_GetProcAddress))
  {
    std::cout << "Failed to initialize the OpenGL context." << std::endl;
    exit(1);
  }
  std::cout << "OpenGL Version " << GLVersion.major << "." << GLVersion.minor << " loaded." << std::endl;

  // configure global opengl state
  // -----------------------------
  glEnable(GL_DEPTH_TEST);

  int nr_attributes;
  glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nr_attributes);
  std::cout << "Maximum nr of vertex attributes supported: " << nr_attributes << std::endl;

  // build and compile our shader program
  // ------------------------------------
  char *data_path = NULL;
  char *base_path = SDL_GetBasePath();
  if (base_path) {
    data_path = base_path;
  } else {
    data_path = SDL_strdup("./");
  }
  std::cout << "SDL BASE PATH: " << data_path << std::endl;

  std::string v_src_path1(data_path), f_src_path1(data_path);
  ShaderProgram lighting_shader(v_src_path1.append("multiple_lights.vsh"), f_src_path1.append("multiple_lights.fsh"));
  std::string v_src_path2(data_path), f_src_path2(data_path);
  ShaderProgram light_cube_shader(v_src_path2.append("light_cube.vsh"), f_src_path2.append("light_cube.fsh"));


  // set up vertex data (and buffer(s)) and configure vertex attributes
  // ------------------------------------------------------------------
  float vertices[] = {
    // positions          // normals           // texture coords
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,
     0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
    -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,

    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,

    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
    -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

     0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,

    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f
  };
  glm::vec3 cubePositions[] = {
    glm::vec3( 0.0f,  0.0f,  0.0f),
    glm::vec3( 2.0f,  5.0f, -15.0f),
    glm::vec3(-1.5f, -2.2f, -2.5f),
    glm::vec3(-3.8f, -2.0f, -12.3f),
    glm::vec3( 2.4f, -0.4f, -3.5f),
    glm::vec3(-1.7f,  3.0f, -7.5f),
    glm::vec3( 1.3f, -2.0f, -2.5f),
    glm::vec3( 1.5f,  2.0f, -2.5f),
    glm::vec3( 1.5f,  0.2f, -1.5f),
    glm::vec3(-1.3f,  1.0f, -1.5f)
  };
  // positions of the point lights
  glm::vec3 pointLightPositions[] = {
    glm::vec3( 0.7f,  0.2f,  2.0f),
    glm::vec3( 2.3f, -3.3f, -4.0f),
    glm::vec3(-4.0f,  2.0f, -12.0f),
    glm::vec3( 0.0f,  0.0f, -3.0f)
  };
  // first, configure the cube's VAO (and VBO)
  unsigned int vbo, cube_vao;
  glGenVertexArrays(1, &cube_vao);
  glGenBuffers(1, &vbo);

  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glBindVertexArray(cube_vao);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
  glEnableVertexAttribArray(2);

  // second, configure the light's VAO (VBO stays the same; the vertices are the same for the light object which is also a 3D cube)
  unsigned int light_cube_vao;
  glGenVertexArrays(1, &light_cube_vao);
  glBindVertexArray(light_cube_vao);

  // we only need to bind to the VBO (to link it with glVertexAttribPointer), no need to fill it; the VBO's data already contains all we need (it's already bound, but we do it again for educational purposes)
  glBindBuffer(GL_ARRAY_BUFFER, vbo);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);

  // load textures (we now use a utility function to keep the code more organized)
  // -----------------------------------------------------------------------------
  std::string light_map_path(data_path), light_map_path2(data_path),light_map_path3(data_path);
  unsigned int diffuse_map = load_texture(light_map_path.append("container2.png").c_str());
  unsigned int specular_map = load_texture(light_map_path2.append("container2_specular.png").c_str());
  // unsigned int emission_map = load_texture(light_map_path3.append("container2_emission.png").c_str());

  // shader configuration
  // --------------------
  lighting_shader.Use(); 
  lighting_shader.SetInt("material.diffuse", 0);
  lighting_shader.SetInt("material.specular", 1);
  // lighting_shader.SetInt("material.emission", 2);

  // Core loop
  while (running)
  {
    // per-frame time logic
    // --------------------
    float current_frame = SDL_GetTicks();
    delta_time = current_frame - last_frame;
    last_frame = current_frame;
    std::cout << "Frame time: " << delta_time << "ms" << std::flush << "\r";

    processInput();
 
    /* do some other stuff here -- draw your app, etc. */
    /* Clear context */
    glClearColor(0.9f, 0.9f, 0.9f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // material properties
    lighting_shader.Use(); 
    // lighting_shader.SetVec3f("material.ambient", 1.0f, 0.5f, 0.31f);
    // lighting_shader.SetVec3f("material.diffuse", 1.0f, 0.5f, 0.31f);
    // lighting_shader.SetVec3f("material.specular", 0.5f, 0.5f, 0.5f); // specular lighting doesn't have full effect on this object's material
    lighting_shader.SetVec3fv("viewPos", camera.position);
    lighting_shader.SetFloat("material.shininess", 32.0f);

    /*
       Here we set all the uniforms for the 5/6 types of lights we have. We have to set them manually and index 
       the proper PointLight struct in the array to set each uniform variable. This can be done more code-friendly
       by defining light types as classes and set their values in there, or by using a more efficient uniform approach
       by using 'Uniform buffer objects', but that is something we'll discuss in the 'Advanced GLSL' tutorial.
    */
    // directional light
    lighting_shader.SetVec3f("dirLight.direction", -0.2f, -1.0f, -0.3f);
    lighting_shader.SetVec3f("dirLight.ambient", 0.05f, 0.05f, 0.05f);
    lighting_shader.SetVec3f("dirLight.diffuse", 0.4f, 0.4f, 0.4f);
    lighting_shader.SetVec3f("dirLight.specular", 0.5f, 0.5f, 0.5f);
    // point light 1
    glm::vec3 diffuse_color = glm::vec3(0.25, 0.93, 0.18);
    lighting_shader.SetVec3fv("pointLights[0].position", pointLightPositions[0]);
    lighting_shader.SetVec3f("pointLights[0].ambient", 0.05f, 0.05f, 0.05f);
    lighting_shader.SetVec3fv("pointLights[0].diffuse", diffuse_color);
    lighting_shader.SetVec3f("pointLights[0].specular", 1.0f, 1.0f, 1.0f);
    lighting_shader.SetFloat("pointLights[0].constant", 1.0f);
    lighting_shader.SetFloat("pointLights[0].linear", 0.09);
    lighting_shader.SetFloat("pointLights[0].quadratic", 0.032);
    // point light 2
    lighting_shader.SetVec3fv("pointLights[1].position", pointLightPositions[1]);
    lighting_shader.SetVec3f("pointLights[1].ambient", 0.05f, 0.05f, 0.05f);
    lighting_shader.SetVec3fv("pointLights[1].diffuse", diffuse_color);
    lighting_shader.SetVec3f("pointLights[1].specular", 1.0f, 1.0f, 1.0f);
    lighting_shader.SetFloat("pointLights[1].constant", 1.0f);
    lighting_shader.SetFloat("pointLights[1].linear", 0.09);
    lighting_shader.SetFloat("pointLights[1].quadratic", 0.032);
    // point light 3
    lighting_shader.SetVec3fv("pointLights[2].position", pointLightPositions[2]);
    lighting_shader.SetVec3f("pointLights[2].ambient", 0.05f, 0.05f, 0.05f);
    lighting_shader.SetVec3fv("pointLights[2].diffuse", diffuse_color);
    lighting_shader.SetVec3f("pointLights[2].specular", 1.0f, 1.0f, 1.0f);
    lighting_shader.SetFloat("pointLights[2].constant", 1.0f);
    lighting_shader.SetFloat("pointLights[2].linear", 0.09);
    lighting_shader.SetFloat("pointLights[2].quadratic", 0.032);
    // point light 4
    lighting_shader.SetVec3fv("pointLights[3].position", pointLightPositions[3]);
    lighting_shader.SetVec3f("pointLights[3].ambient", 0.05f, 0.05f, 0.05f);
    lighting_shader.SetVec3fv("pointLights[3].diffuse", diffuse_color);
    lighting_shader.SetVec3f("pointLights[3].specular", 1.0f, 1.0f, 1.0f);
    lighting_shader.SetFloat("pointLights[3].constant", 1.0f);
    lighting_shader.SetFloat("pointLights[3].linear", 0.09);
    lighting_shader.SetFloat("pointLights[3].quadratic", 0.032);
    // spotLight
    lighting_shader.SetVec3fv("spotLight.position", camera.position);
    lighting_shader.SetVec3fv("spotLight.direction", camera.front);
    lighting_shader.SetVec3f("spotLight.ambient", 0.0f, 0.0f, 0.0f);
    lighting_shader.SetVec3f("spotLight.diffuse", 1.0f, 1.0f, 1.0f);
    lighting_shader.SetVec3f("spotLight.specular", 1.0f, 1.0f, 1.0f);
    lighting_shader.SetFloat("spotLight.constant", 1.0f);
    lighting_shader.SetFloat("spotLight.linear", 0.09);
    lighting_shader.SetFloat("spotLight.quadratic", 0.032);
    lighting_shader.SetFloat("spotLight.cutOff", glm::cos(glm::radians(12.5f)));
    lighting_shader.SetFloat("spotLight.outerCutOff", glm::cos(glm::radians(15.0f)));

    // view/projection transformations
    glm::mat4 projection = camera.ProjectionMatrix();
    glm::mat4 view = camera.ViewMatrix();
    lighting_shader.SetMat4f("projection", projection);
    lighting_shader.SetMat4f("view", view);

    // world transformation
    glm::mat4 model = glm::mat4(1.0f);
    lighting_shader.SetMat4f("model", model);

    // bind diffuse map
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, diffuse_map);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, specular_map);
    // glActiveTexture(GL_TEXTURE2);
    // glBindTexture(GL_TEXTURE_2D, emission_map);

    glBindVertexArray(cube_vao);
    for (unsigned int i = 0; i < 10; i++)
    {
      glm::mat4 model = glm::mat4(1.0f);
      model = glm::translate(model, cubePositions[i]);
      float angle = 20.0f * i;
      model = glm::rotate(model, glm::radians(angle) + current_frame/1000.0f, glm::vec3(1.0f, 0.3f, 0.5f));
      lighting_shader.SetMat4f("model", model);

      glDrawArrays(GL_TRIANGLES, 0, 36);
    }

    // also draw the lamp object
    light_cube_shader.Use();
    light_cube_shader.SetMat4f("projection", projection);
    light_cube_shader.SetMat4f("view", view);
    // light_pos.x = sin(current_frame / 1000.0f) * 2.0f;
    // light_pos.z = cos(current_frame / 1000.0f) * 2.0f;
    // model = glm::mat4(1.0f);
    // model = glm::translate(model, light_pos);
    // model = glm::scale(model, glm::vec3(0.2f)); // a smaller cube
    // light_cube_shader.SetMat4f("model", model);

    // glBindVertexArray(light_cube_vao);
    // glDrawArrays(GL_TRIANGLES, 0, 36);

    // we now draw as many light bulbs as we have point lights.
    glBindVertexArray(light_cube_vao);
    for (unsigned int i = 0; i < 4; i++)
    {
      model = glm::mat4(1.0f);
      model = glm::translate(model, pointLightPositions[i]);
      model = glm::scale(model, glm::vec3(0.2f)); // Make it a smaller cube
      light_cube_shader.SetMat4f("model", model);
      glDrawArrays(GL_TRIANGLES, 0, 36);
    }

    /* Swap our buffer to display the current contents of buffer on screen */ 
    SDL_GL_SwapWindow(window);
  }

  glDeleteVertexArrays(1, &cube_vao);
  glDeleteVertexArrays(1, &light_cube_vao);
  glDeleteBuffers(1, &vbo);

  // Once finished with OpenGL functions, the SDL_GLContext can be deleted.
  SDL_GL_DeleteContext(gl_context);  

  // Close and destroy the window
  SDL_DestroyWindow(window);

  // Clean up
  SDL_Quit();
  return 0;
}
