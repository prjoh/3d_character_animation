#pragma once


#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include "ShaderProgram.h"


class DebugDrawer {
  public:
    DebugDrawer(ShaderProgram &shader);
    void SetMatrices(glm::mat4 &view_matrix, glm::mat4 &projection_matrix);
    void DrawLine(glm::vec3 &from, glm::vec3 &to, glm::vec3 &color);
  private:
    ShaderProgram shader_program;
};
