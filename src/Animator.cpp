#include "Animator.h"


Animator::Animator()
{

}

glm::mat4 Animator::GetGlobalTransform(Joint &joint)
{
  if (joint.parent_index > -1)
    return GetGlobalTransform(joints[joint.parent_index]) * joint.transform;
  else
    return joint.transform;
}
