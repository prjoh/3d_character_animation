#pragma once


#include <vector>
#include <map>
#include <set>
#include <unordered_set>
#include <string>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <glm/ext/quaternion_float.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Mesh.h"
#include "Animator.h"


class ModelLoader
{
  public:
    std::string directory;

    ModelLoader(const std::string &path);
    void LoadMeshes(std::vector<Mesh> &meshes);
    void LoadAnimations(Animator &animator);
  private:
    Assimp::Importer importer;
    const aiScene *scene;
    std::vector<Texture_Util::Texture> textures_loaded;  // stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.

    void ProcessMeshNode(aiNode *node, const aiScene *scene, std::vector<Mesh> &meshes);
    Mesh ProcessMesh(aiMesh *mesh, const aiScene *scene);
    std::vector<Texture_Util::Texture> LoadMaterialTextures(aiMaterial *mat, const aiScene *scene, aiTextureType type, std::string type_name);
    void ProcessJointNode(aiNode *node, const aiScene *scene, std::vector<Joint> &joints, int parent_index);

    static inline glm::vec3 vec3_cast(const aiVector3D &v) { return glm::vec3(v.x, v.y, v.z); }
    static inline glm::vec2 vec2_cast(const aiVector3D &v) { return glm::vec2(v.x, v.y); } // it's aiVector3D because assimp's texture coordinates use that
    static inline glm::quat quat_cast(const aiQuaternion &q) { return glm::quat(q.w, q.x, q.y, q.z); }
    static inline glm::mat4 mat4_cast(const aiMatrix4x4 &m) { return glm::transpose(glm::make_mat4(&m.a1)); }
};