#include "Texture.h"


namespace Texture_Util
{
  unsigned int load_texture_from_file(char const * path)
  {
    unsigned int texture_id;
    glGenTextures(1, &texture_id);
    
    int width, height, nr_components;
    unsigned char *data = stbi_load(path, &width, &height, &nr_components, 0);
    if (data)
    {
      GLenum format;
      if (nr_components == 1)
        format = GL_RED;
      else if (nr_components == 3)
        format = GL_RGB;
      else if (nr_components == 4)
        format = GL_RGBA;
      else
      {
        std::cout << "Invalid number of components in texture: " << path << std::endl;
        stbi_image_free(data);
        return texture_id;
      }

      glBindTexture(GL_TEXTURE_2D, texture_id);
      glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
      glGenerateMipmap(GL_TEXTURE_2D);

      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

      stbi_image_free(data);
    }
    else
    {
      std::cout << "Texture failed to load at path: " << path << std::endl;
      stbi_image_free(data);
    }

    return texture_id;
  }

  unsigned int load_texture_from_memory(const aiTexture *texture)
  {
    unsigned int texture_id;
    glGenTextures(1, &texture_id);

    int width, height, nr_components;
    unsigned char *data = nullptr;
    if (texture->mHeight == 0)
      data = stbi_load_from_memory(reinterpret_cast<unsigned char*>(texture->pcData), texture->mWidth, &width, &height, &nr_components, 0);
    else
      data = stbi_load_from_memory(reinterpret_cast<unsigned char*>(texture->pcData), texture->mWidth * texture->mHeight, &width, &height, &nr_components, 0);

    if (data)
    {
      GLenum format;
      if (nr_components == 1)
        format = GL_RED;
      else if (nr_components == 3)
        format = GL_RGB;
      else if (nr_components == 4)
        format = GL_RGBA;
      else
      {
        std::cout << "Invalid number of components in memory texture." << std::endl;
        stbi_image_free(data);
        return texture_id;
      }

      glBindTexture(GL_TEXTURE_2D, texture_id);
      glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
      glGenerateMipmap(GL_TEXTURE_2D);

      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

      stbi_image_free(data);
    }
    else
    {
      std::cout << "Texture failed to load from memory." << std::endl;
      stbi_image_free(data);
    }

    return texture_id;
  }
}
