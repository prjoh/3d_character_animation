#pragma once


#include <iostream>
#include <glad/glad.h>
#include <stb/stb_image.h>
#include <assimp/scene.h>


namespace Texture_Util
{
  struct Texture
  {
    unsigned int id;
    std::string type;
    std::string path;
  };

  unsigned int load_texture_from_file(char const *path);
  unsigned int load_texture_from_memory(const aiTexture *texture);
}
