#pragma once


#include "Mesh.h"
#include "ModelLoader.h"
#include "Animator.h"
#include "DebugDrawer.h"


class Model
{
  public:
    std::vector<Mesh> meshes;
    Animator animator;
    bool gamma_correction;

    Model(ModelLoader &model_loader, bool gamma = false);
    void Draw(ShaderProgram &shader);

    // DEBUG
    std::map<int, Mesh> skeleton;
    void DrawSkeleton(DebugDrawer &debug, glm::mat4 model_transform, float delta_time);
};
