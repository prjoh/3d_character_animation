#pragma once


#include <string>
#include <vector>
#include <glm/vec3.hpp>
#include <glm/ext/quaternion_float.hpp>


struct Joint
{
  int joint_id;
  int parent_index;
  std::string name;
  glm::mat4 transform;
  glm::mat4 offset_matrix;
};

struct JointKeyframe
{
  int joint_id;
  glm::vec3 translation;
  glm::vec3 scale;
  glm::quat rotation;
};

struct Animation
{
  std::string name;
  unsigned int duration;
  unsigned int ticks_per_second;
  std::vector<double> times;
  std::vector<std::vector<JointKeyframe>> keyframes;
};

class Animator
{
  public:
    std::vector<Joint> joints;
    std::vector<glm::mat4> bone_matrices;
    std::vector<Animation> animations;

    Animator();
    glm::mat4 GetGlobalTransform(Joint &joint);
  private:
    void ProcessJointTransform();
};