#include "Mesh.h"


using namespace Texture_Util;


Mesh::Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures)
  : vertices(vertices), indices(indices), textures(textures)
{
  // now that we have all the required data, set the vertex buffers and its attribute pointers.
  SetupMesh();
}

void Mesh::Draw(ShaderProgram &shader)
{
  // bind appropriate textures
  unsigned int diffuse_nr  = 1;
  unsigned int specular_nr = 1;
  unsigned int emission_nr = 1;
  unsigned int normal_nr   = 1;
  unsigned int height_nr   = 1;
  for (unsigned int i = 0; i < this->textures.size(); ++i)
  {
    glActiveTexture(GL_TEXTURE0 + i); // active proper texture unit before binding
    // retrieve texture number (the N in diffuse_textureN)
    std::string number;
    std::string name = this->textures[i].type;
    if (name == "texture_diffuse")
      number = std::to_string(diffuse_nr++);
    else if (name == "texture_specular")
      number = std::to_string(specular_nr++); // transfer unsigned int to stream
    else if (name == "texture_emission")
      number = std::to_string(emission_nr++); // transfer unsigned int to stream    
    else if (name == "texture_normal")
      number = std::to_string(normal_nr++); // transfer unsigned int to stream
     else if (name == "texture_height")
      number = std::to_string(height_nr++); // transfer unsigned int to stream

    // now set the sampler to the correct texture unit
    glUniform1i(glGetUniformLocation(shader.id, (name + number).c_str()), i);
    // and finally bind the texture
    glBindTexture(GL_TEXTURE_2D, this->textures[i].id);
  }
  
  // draw mesh
  glBindVertexArray(this->vao);
  glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);

  // always good practice to set everything back to defaults once configured.
  glActiveTexture(GL_TEXTURE0);
}

void Mesh::SetupMesh(void)
{
  // create buffers/arrays
  glGenVertexArrays(1, &this->vao);
  glGenBuffers(1, &this->vbo);
  glGenBuffers(1, &this->ebo);

  glBindVertexArray(vao);
  // load data into vertex buffers
  glBindBuffer(GL_ARRAY_BUFFER, this->vbo);
  // A great thing about structs is that their memory layout is sequential for all its items.
  // The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
  // again translates to 3/2 floats which translates to a byte array.
  glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);  

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(unsigned int), &this->indices[0], GL_STATIC_DRAW);

  // set the vertex attribute pointers
  // vertex Positions
  glEnableVertexAttribArray(0); 
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
  // vertex normals
  glEnableVertexAttribArray(1); 
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
  // vertex texture coords
  glEnableVertexAttribArray(2); 
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, tex_coords));
  // vertex tangent
  glEnableVertexAttribArray(3);
  glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, tangent));
  // vertex bitangent
  glEnableVertexAttribArray(4);
  glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, bitangent));
  // vertex bone indices
  glEnableVertexAttribArray(5);
  glVertexAttribIPointer(5, NUM_BONES_PER_VERTEX, GL_INT, sizeof(Vertex), (void*)offsetof(Vertex, bone_indices));
  // vertex bone weights
  glEnableVertexAttribArray(6);
  glVertexAttribPointer(6, NUM_BONES_PER_VERTEX, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, bone_weights));

  glBindVertexArray(0);
}