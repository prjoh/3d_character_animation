#include "PerspectiveCamera.h"


PerspectiveCamera::PerspectiveCamera(glm::vec3 position, float fov, float aspect, float near, float far)
  : position(position),
    near(near),
    far(far),
    aspect(aspect),
    fov(fov)
{
  this->world_up = glm::vec3(0.0f, 1.0f, 0.0f);
  this->up = world_up;
  this->front = glm::vec3(0.0f, 0.0f, -1.0f);
  this->right = glm::vec3(1.0f, 0.0f, 0.0f);
  this->yaw_angle = -90.0f;
  this->pitch_angle = 0.0f;
}

void PerspectiveCamera::Move(glm::vec3 &distance)
{
  this->position += distance;
}

void PerspectiveCamera::SetPosition(glm::vec3 &new_position)
{
  this->position = new_position;
}

void PerspectiveCamera::AddEulerRotation(float d_yaw, float d_pitch, bool constrain_pitch)
{
  this->yaw_angle += d_yaw;
  this->pitch_angle += d_pitch;

  if (constrain_pitch)
  {
    if (pitch_angle > 89.0f)
      pitch_angle = 89.0f;
    if (pitch_angle < -89.0f)
      pitch_angle = -89.0f;
  }

  // Update view matrix
  this->front.x = cos(glm::radians(yaw_angle)) * cos(glm::radians(pitch_angle));
  this->front.y = sin(glm::radians(pitch_angle));
  this->front.z = sin(glm::radians(yaw_angle)) * cos(glm::radians(pitch_angle));
  this->front = glm::normalize(front);
  this->right = glm::normalize(glm::cross(front, world_up));  // normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
  this->up    = glm::normalize(glm::cross(right, front));
}

void PerspectiveCamera::SetAspect(float new_aspect)
{
  this->aspect = new_aspect;
}

void PerspectiveCamera::SetFOV(float new_fov)
{
  this->fov = new_fov;
}

glm::mat4 PerspectiveCamera::ViewMatrix(void)
{
  return glm::lookAt(this->position, this->position + this->front, this->up);
}

glm::mat4 PerspectiveCamera::ProjectionMatrix(void)
{
  return glm::perspective(glm::radians(this->fov), this->aspect, this->near, this->far);
}
