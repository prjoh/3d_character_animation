#pragma once


#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <glad/glad.h>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat2x2.hpp>
#include <glm/mat3x3.hpp>
#include <glm/mat4x4.hpp>


class ShaderProgram
{
  public:
    unsigned int id;
    ShaderProgram(std::string &v_shader_src_path, std::string &f_shader_src_path);
    void Use(void);
    void SetInt(const std::string &name, int data) const;
    void SetFloat(const std::string &name, float data) const;
    void SetVec3fv(const std::string &name, const glm::vec3 &data) const;
    void SetVec3f(const std::string &name, float x, float y, float z) const;
    void SetVec4fv(const std::string &name, const glm::vec4 &data) const;
    void SetVec4f(const std::string &name, float x, float y, float z, float w) const;
    void SetMat2f(const std::string &name, glm::mat2 &data) const;
    void SetMat3f(const std::string &name, glm::mat3 &data) const;
    void SetMat4f(const std::string &name, glm::mat4 &data) const;
  private:
    void CheckCompileErrors(unsigned int shader, std::string type);
};
