#pragma once


#include <string>
#include <vector>
#include <glm/vec3.hpp>

#include "ShaderProgram.h"
#include "Texture.h"


#define NUM_BONES_PER_VERTEX 4


struct Vertex
{
  glm::vec3 position;
  glm::vec3 normal;
  glm::vec2 tex_coords;
  glm::vec3 tangent;
  glm::vec3 bitangent;
  unsigned int bone_indices[NUM_BONES_PER_VERTEX];
  float bone_weights[NUM_BONES_PER_VERTEX];
};

class Mesh
{
  public:
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<Texture_Util::Texture> textures;
    unsigned int vao;

    Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture_Util::Texture> textures);
    void Draw(ShaderProgram &shader);

  private:
    unsigned int vbo, ebo;
    
    // initializes all the buffer objects/arrays
    void SetupMesh(void);
};